package approcheobjet;

import approcheobjet.birds.*;

public class AppBird {
	
	public static void main( String[] args ) {
		
		Bird[] birds = {new Dove( "dove" ), new Duck( "donald" ), new Penguin( "Piglou" )};
		ICanFly[] flyingBirds = {new Dove( "dove" ), new Duck( "donald" )};
		for(Bird bird : birds) {
			bird.describe();
		}
		
		for(ICanFly bird : flyingBirds ) {
			bird.fly();
			bird.land();
			if ( bird instanceof Dove) {
				((Dove)bird).mDove();
			}
		}
	}
}
