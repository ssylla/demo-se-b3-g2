package approcheobjet.exception;

public class App {
	
	public static void main( String[] args ) {
		System.out.println("main begin");
		try {
			m1();
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		System.out.println("main end");
	}
	
	public static void m1 () throws Exception {
		System.out.println("m1 begin");
		try {
			m2();
		} catch ( ArithmeticException e ) {
			System.out.println("mon traitement");
			throw e;
		}
		System.out.println("m1 end");
	}
	
	private static void m2() throws ArithmeticException {
		System.out.println("m2 begin");
		int diviseur = ( int ) (Math.random() * 2);
		System.out.println("la division de 1/"+diviseur+" = "+ 1/diviseur);
		System.out.println("m2 end");
	}
}
