package approcheobjet.domain;

import java.util.Objects;

public class Contact {
	
	private String name;
	private String email;
	private String tel;
	
	public Contact() {
		tel = "02.41.74.74.74";
	}
	
	public Contact( String name, String email ) {
		this();
		this.name = name;
		this.email = email;
	}
	
	public Contact( String name, String email, String tel ) {
		this( name, email );
		this.tel = tel;
	}
	
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getTel() {
		return tel;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Contact{" );
		sb.append( "name='" ).append( name ).append( '\'' );
		sb.append( ", email='" ).append( email ).append( '\'' );
		sb.append( ", tel='" ).append( tel ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
	
	@Override
	public boolean equals( Object o ) {
		if ( this == o ) return true;
		if ( !(o instanceof Contact) ) return false;
		Contact contact = ( Contact ) o;
		return name.equals( contact.name ) && email.equals( contact.email );
	}
	
	@Override
	public int hashCode() {
		return Objects.hash( name, email );
	}
}
