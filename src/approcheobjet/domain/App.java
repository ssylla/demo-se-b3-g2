package approcheobjet.domain;

import java.util.Arrays;

public class App {
	
	public static void main( String[] args ) {
		
		Contact sega = new Contact( "sega", "s@s.fr" );
		//System.out.println(sega.toString());
		System.out.println(sega);
		
		ContactPro pro = new ContactPro("ssylla");
		System.out.println(pro);
	}
}
