package approcheobjet.collections;

import fr.epsi.cclib.CSetArray;
import fr.epsi.cclib.ICSet;
import fr.epsi.cclib.exception.CSetEmptyException;
import fr.epsi.cclib.exception.CSetGenericException;
import fr.epsi.cclib.exception.CSetIndexOutOfBoundException;

public class TestCSet {
	
	public static void main( String[] args ) throws CSetGenericException {
		
		try {
			traitement(15);
		} catch ( CSetEmptyException e ) {
			e.printStackTrace();
		} catch ( CSetIndexOutOfBoundException e ) {
			System.err.println(e.getMessage());
		}
	}
	
	private static void traitement(int i) throws CSetEmptyException, CSetIndexOutOfBoundException {
		ICSet set = new CSetArray();
		try {
			set.add( 10 );
			set.add( "test" );
		} catch ( CSetGenericException e ) {
			e.printStackTrace();
		}
		set.remove( 3 );
		System.out.println(i);
		++i;
	}
	
	
}
