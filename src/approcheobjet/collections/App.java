package approcheobjet.collections;

import approcheobjet.birds.*;

import java.text.SimpleDateFormat;
import java.util.*;

public class App {
	
	public static void main( String[] args ) {
		
		List<String> list = new ArrayList<>();
		list.add( "toto" );
		list.add( "titi" );
		list.remove( "toto" );
		
		System.out.println( list.size() );
		
		
		List<Bird> birdsList = new ArrayList<>();
		birdsList.add( new Dove( "Peace", 10 ) );
		birdsList.add( new Duck( "Donald", 60 ) );
		birdsList.add( new Duck( "Donald", 45 ) );
		
		Collections.sort( birdsList, new BirdAgeComparator() );
		
		//Parcours foreach
		for ( Bird item : birdsList ) {
			System.out.println(item);
		}
		
		Collections.sort( birdsList, new BirdNameComparator() );
		
		//Parcours foreach
		for ( Bird item : birdsList ) {
			System.out.println(item);
		}
		
		//Parcours iterator
		Iterator<Bird> iterator = birdsList.iterator();
		while ( iterator.hasNext() ) {
			Bird item = iterator.next();
			item.describe();
		}
		
		//Parcours index
		for ( int i = 0, size = birdsList.size(); i < size; ++i ) {
			birdsList.get( i ).describe();
		}
		
		int a = 2;
		System.out.println( ++a + " - " + (++a + 2) + " - " + a++ );
		System.out.println( a );
		//3 - 6 - 6
		//3 - 4 - 6
		//3 - 4 - 5
		//3 - 6 - 4
		//3 - 42 - 6
		
		System.out.println( Integer.MAX_VALUE );
		
		List<Integer> tabInt = new ArrayList<>();
		tabInt.add( 12 );
		
		Integer i = new Integer( 12 );
		Integer ii = 12;
		String str = "toto";
		String str2 = "toto";
		// String str2 = new String("toto");
		
		if ( str == str2 ) {
			System.out.println( "égalité" );
		} else {
			System.out.println( "différence" );
		}
		
		
		String s1 = new String( "hello" );
		String s2 = s1 + (" world");
		String s3 = s1.concat( " world" );
		
		String s = "bonjour";
		s += " la";
		s += " classe b3";
		
		System.out.println(s2.equals( s3 ));
		
		
		for (String arg : args  ) {
			System.out.println(arg);
		}
		
		Arrays.asList( args ).forEach( System.out::println );
	}
}
