package approcheobjet.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AppGeneric {
	
	public static void main( String[] args ) {
		
		List objectsList = new ArrayList();
		objectsList.add( 1 );
		objectsList.add( "string" );
		
		for ( Object o : objectsList ) {
			if ( o instanceof String ) {
				System.out.println((( String ) o).toUpperCase());
			}
		}
		
		List<String> stringsList = new ArrayList<>();
		stringsList.add( "string" );
		stringsList.add( "str" );
		
		for ( String item : stringsList ) {
			System.out.println(item.toUpperCase());
		}
		Iterator<String> iterator = stringsList.iterator();
		
		
	}
	
	private static void m( int i ) {
		System.out.println(i);
	}
	
	
}
