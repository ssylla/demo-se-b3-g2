package approcheobjet.birds;

public class Dove extends Bird implements ICanFly {
	public Dove( String name ) {
		super( name );
	}
	
	public Dove( String name, int age ) {
		super( name, age );
	}
	
	@Override
	public void describe() {
		System.out.println( "i am a dove named: " + name );
	}
	
	@Override
	public void fly() {
		System.out.println( "i fly like a dove" );
	}
	
	public void mDove() {
		System.out.println("My dove method");
	}
}
