package approcheobjet.birds;

import java.util.Comparator;

public class BirdAgeComparator implements Comparator<Bird> {
	@Override
	public int compare( Bird o1, Bird o2 ) {
		return o1.age - o2.age;
	}
}
