package approcheobjet.birds;

public abstract class Bird implements Comparable<Bird> {
	
	protected String name;
	protected int age;
	
	public Bird( String name ) {
		this.name = name;
	}
	
	public Bird( String name, int age ) {
		this.name = name;
		this.age = age;
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public abstract void describe();
	
	@Override
	public int compareTo( Bird otherBird ) {
		int result = name.compareTo( otherBird.name );
		if ( result == 0 ) {
			result = age - otherBird.age;
		}
		return result;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Bird{" );
		sb.append( "name='" ).append( name ).append( '\'' );
		sb.append( ", age=" ).append( age );
		sb.append( '}' );
		return sb.toString();
	}
}
